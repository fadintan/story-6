# PPW Stories
* Intan Fadilla Andyani
* 1806141246
* PPW-B
* Story6 - Story9
* Project Name: story6

## Story 6
- App Name: [whatsgood](https://gitlab.com/fadintan/story-6/tree/master/whatsgood)
- Status Page
- URL: [Hello!](http://story6-intan.herokuapp.com/)

## Story 7
- App Name: [story7](https://gitlab.com/fadintan/story-6/tree/master/story7)
- Accordions
- URL: [About](http://story6-intan.herokuapp.com/about/)

## Story 8
- App Name: [story8](https://gitlab.com/fadintan/story-6/tree/master/story8)
- Books
- URL: [Books](http://story6-intan.herokuapp.com/books/)

## Story 9
- App Name: [story9](https://gitlab.com/fadintan/story-6/tree/master/story9)
- Login
- URL: [Login](http://story6-intan.herokuapp.com/login/)