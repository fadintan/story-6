from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User

from . import views

class UnitTest(TestCase):
    # Test URL & HTML
    def test_url_login_is_exist(self):
        resp = Client().get('/login/')
        self.assertEqual(resp.status_code, 200)

    def test_login_template(self):
        resp = Client().get('/login/')
        self.assertTemplateUsed(resp, 'login.html')

    def test_welcome_page_is_exist(self):
        resp = Client().get('/welcome/')
        self.assertEqual(resp.status_code, 302)
    


    def test_logout_page_is_exist(self):
        resp = Client().get('/logout/')
        self.assertEqual(resp.status_code, 200)
    
    def test_logout_page_template(self):
        resp = Client().get('/logout/')
        self.assertTemplateUsed(resp, 'logout.html')

    def test_error_url(self):
        resp = Client().get('/masuk/')
        self.assertEqual(resp.status_code, 404)

    # Test Views
    def test_logout_page_using_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, views.logout_view)
    
    def test_login_page_using_func(self):
        found = resolve('/welcome/')
        self.assertEqual(found.func, views.welcome)
    
    def test_login_page_using_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, views.login_view)

    def test_login_logout(self):
        client = Client()

        username = 'test'
        password = 'bismillah123'
        user = User.objects.create_user(username=username, password=password)
        
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        # Test if login successful
        response = client.get('/welcome/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Hello', response.content.decode())
        
        # Logout
        response = client.get('/logout/')
  
