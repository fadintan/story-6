from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
 

def signUp(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/welcome/')
    if request.method == 'GET':
        form = UserCreationForm()
        return render(request, 'signup.html', {'form' : form})
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username = username, password = password)

            return HttpResponseRedirect('/login/')
        else:
            return render(request, 'signup.html', {'form' : form})

def login_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/welcome/')
    if request.method == 'GET':
        form = AuthenticationForm()
        return render(request, 'login.html', {'form': form})
    if request.method == 'POST':
        form = AuthenticationForm(request = request, data = request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username = username, password = password)
           
            if user is not None:
                print(user)
                login(request, user)
                return HttpResponseRedirect('/welcome/')
            else:
                # maunya pop up tulisan 'User not found'
                print('Maunya pop up tulisan user not found')
        else:
            return render(request, 'login.html', {'form': form})

@login_required
def welcome(request):
    return render(request, 'welcome.html', {})

def logout_view(request):
    logout(request)
    return render(request, 'logout.html', {})