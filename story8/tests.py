from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views

# Create your tests here.

class UnitTest(TestCase):
    def test_url_books_is_exist(self):
        resp = Client().get('/books/')
        self.assertEqual(resp.status_code, 200)

    def test_books_template(self):
        resp = Client().get('/books/')
        self.assertTemplateUsed(resp, 'books.html')
    
    def test_error_url(self):
        resp = Client().get('/akucapek/')
        self.assertEqual(resp.status_code, 404)

    def test_page_using_func(self):
        resp = resolve('/books/')
        self.assertEqual(resp.func, views.books)