from django.shortcuts import render, redirect
from .models import *
from .forms import *

# Create your views here.

def hello(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            mystatus = Status(text=form.data['text'])
            mystatus.save()
            return redirect('/')
    else:
        form = StatusForm()

    context={'status' : Status.objects.all().values(),
    'form':form}
    return render(request, "hello.html", context)