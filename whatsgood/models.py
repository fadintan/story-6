from django.conf import settings
from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    text = models.CharField(max_length=300)
    created_date = models.DateTimeField(auto_now_add = True, blank=True)

    def __str__(self):
        return self.text