from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve, reverse
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from . import models, views
from selenium import webdriver
import unittest
import time


class MyFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(MyFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(MyFunctionalTest, self).tearDown()

    def test_add_status(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)

        status = self.browser.find_element_by_name('text')
        status.send_keys('Coba Coba')

        submit = self.browser.find_element_by_name('Submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn('Coba Coba', self.browser.page_source)

class MyUnitTest(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_is_not_exist(self):
        response = Client().get('/error/')
        self.assertEqual(response.status_code, 404)

    def test_form_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</form>', self.response.content.decode())

    def test_apa_kabar_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</h1>', self.response.content.decode())

    def test_page_using_whatsgood_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_page_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.hello)

    
    def test_models_str_and_count(self):
        prev = models.Status.objects.all().count()
        status = models.Status.objects.create(text="Hmm")
        current = models.Status.objects.all().count()
        self.assertEqual("Hmm", status.__str__())
        self.assertEqual(prev+1, current)