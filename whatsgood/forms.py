from django import forms
from .models import Status

class StatusForm(forms.Form):
    text = forms.CharField(label='', required=True, max_length=300, 
    widget = forms.Textarea(attrs = {'class' : 'form-control form-control-sm', 'style' : 'height: 100px;', 'placeholder' : 'What is on your mind..'}),)

    class Meta:
        model = Status
        fields = ('text',)