$(document).ready(function() {
  $(".accordion").on("click", ".accordion-header", function() {
    $(this).toggleClass("active").next().slideToggle();
  });
});


function setCookie(cname, cvalue, exdays = 365) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		  c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function darkMode(){
  $(".navbar a").css("color", "whitesmoke");  
  $(".dropdown-menu a").css("color", "black");
  $("body").css("background-color", "#212F3C");
  $(".intan").css("color", "#F5F5F5");
  $(".bookname").css("color", "#F5F5F5");
  $(".accordion").css("background", "#C0C0C0");
  $(".accordion-header").css("border-bottom", "#212F3C");
  $(".accordion-header").css("color", "#212F3C");
  $(".accordion-content").css("background", "#C0C0C0");
  $(".accordion-content").css("color", "#212F3C");
  $(".halo").css("color", "#F5F5F5");
  $(".card").css("background-color", "#C0C0C0");
  $(".card-body").css("color", "#212F3C")
  $(".btn").addClass("btn-outline-light");
  $(".btn").removeClass("btn-outline-dark");
  setCookie("colormode", "dark");
}

function lightMode(){
  $(".navbar a").css("color", "black");
  $(".dropdown-menu a").css("color", "black");
  $("body").css("background-color", "white");
  $(".intan").css("color", "#212529");
  $(".bookname").css("color", "#212529");
  $(".accordion").css("background", "#212F3C");
  $(".accordion-header").css("border-bottom", "whitesmoke");
  $(".accordion-header").css("color", "whitesmoke");
  $(".accordion-content").css("background", "#212F3C");
  $(".accordion-content").css("color", "whitesmoke");
  $(".halo").css("color", "#696969");
  $(".card").css("background-color", "#212F3C");
  $(".card-body").css("color", "whitesmoke")
  $(".btn").addClass("btn-outline-dark");
  $(".btn").removeClass("btn-outline-light");
  setCookie("colormode", "light");
}

if (getCookie("colormode") === "light") {
	lightMode();
} else if (getCookie("colormode") === "dark") {
	darkMode();
}

$(document).ready(function() {
	$(".light").click(lightMode);
	$(".dark").click(darkMode);
});