from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from . import views
import time


# Create your tests here.
class UnitTest(TestCase):
    def test_about_page_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_page_using_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_page_using_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, views.about)

class FunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

'''
    def test_dark_theme(self):
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(1)

        dropdown= self.browser.find_element_by_css_selector('.dropdown-toggle')
        dropdown.send_keys(Keys.RETURN)
        time.sleep(2)
        theme = self.browser.find_element_by_css_selector('.dark')
        theme.send_keys(Keys.RETURN)
        time.sleep(2)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, "rgba(33, 47, 60, 1)")
        self.tearDown()
        '''
